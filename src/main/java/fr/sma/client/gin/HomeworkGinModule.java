package fr.sma.client.gin;

import com.google.gwt.inject.client.AbstractGinModule;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

/**
 * Created by mansri.s on 21/01/2015.
 */
public class HomeworkGinModule extends AbstractGinModule{
    @Override
    protected void configure() {
        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
    }

    @Provides
    @Singleton
    PlaceController providePlaceController(EventBus eventBus){
        return new PlaceController(eventBus);
    }
}
