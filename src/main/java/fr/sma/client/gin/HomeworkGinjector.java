package fr.sma.client.gin;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import fr.sma.client.Messages;
import fr.sma.client.ui.HelloScreen;


/**
 * Created by mansri.s on 21/01/2015.
 */
@GinModules(HomeworkGinModule.class)
public interface HomeworkGinjector extends Ginjector{
    HelloScreen getHelloScreen();
}
