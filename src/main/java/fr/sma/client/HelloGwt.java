package fr.sma.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import fr.sma.client.gin.HomeworkGinjector;
import fr.sma.client.ui.HelloScreen;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class HelloGwt implements EntryPoint {

  private final HomeworkGinjector ginjector = GWT.create(HomeworkGinjector.class);

  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
    HelloScreen helloScreen = ginjector.getHelloScreen();
    RootPanel.get().add(helloScreen);
    helloScreen.selectNameField();
  }
}
