package fr.sma.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

/**
 * Created by Seifoun on 25/11/2014.
 */
public interface Resources extends ClientBundle{

    public static final Resources INSTANCE = GWT.create(Resources.class);

    @Source("helloScreen.css")
    Style style();

    public interface Style extends CssResource{

        String nameField();

        String errorLabel();

        String footer();
    }
}
