package fr.sma.client;

import com.google.gwt.core.client.GWT;

public interface Messages extends com.google.gwt.i18n.client.Messages {

    public static final Messages INSTANCE = GWT.create(Messages.class);

    @DefaultMessage("Web Application Starter Project")
    String screenTitle();

    @DefaultMessage("Please enter your name:")
    String nameFieldLabel();

    @DefaultMessage("Enter your name")
    String nameField();

    @DefaultMessage("Send")
    String sendButton();

    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    @DefaultMessage("An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.")
    String serverError();

    @DefaultMessage("Copyright SMANSRI Janvier 2015")
    String footer();
}
